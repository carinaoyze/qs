<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListaController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FormController;
use App\Http\Controllers\DependentesController;
use App\Models\User;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index',  ['menu' => 'Menu de Opções', 
    'inicio' => 'Início', 
    'listar' => 'Listar Cadastros', 
    'incluir' => 'Incluir Novo']);
   
});

Route::get('/index', function () {
    return view('index', ['menu' => 'Menu de Opções', 
    'inicio' => 'Início', 
    'listar' => 'Listar Cadastros', 
    'incluir' => 'Incluir Novo']);
   
});

Route::get('/recebido', function () {
    return view('recebido',  ['menu' => 'Menu de Opções', 
    'inicio' => 'Início', 
    'listar' => 'Listar Cadastros', 
    'incluir' => 'Incluir Novo']);
   
});

//Route::resource('form', FormController::class);

Route::resource('form', FormController::class);
   
Route::get('/lista',  [ListaController::class, 'render']);

Route::resource('dependentes', DependentesController::class);
   
Route::delete('lista', [ListaController::class, 'destroy']);

Route::delete('lista', [ListaController::class, 'deleteAll']);

Route::get('/recebido', function () {
    return view('recebido',  ['menu' => 'Menu de Opções', 
    'inicio' => 'Início', 
    'listar' => 'Listar Cadastros', 
    'incluir' => 'Incluir Novo']);
   
}); 



