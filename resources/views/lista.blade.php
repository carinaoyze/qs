<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('principal.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<title>Cadastro de Pessoas</title>
</head>

<body>

<div id="conteudoGeral">

    <div id="topoGeral">
    	<div id="logoTopo" onclick="location.href='index'" style="cursor:pointer;"></div>
    	<div id="dirTopo"></div>
    </div>
    
    <div id="baixoGeral">
    
    	<div id="menuEsq">
            <div id="titMenu">{{$menu}}</div>
            <a href="index">{{$inicio}}</a> 
            <a href="lista">{{$listar}}</a>
            <a href="{{ route ('form.create') }}"> {{$incluir}}</a>
        </div>
        
        <div id="conteudoDir">

            <div id="listaPessoas">
            	<h1>Cadastros</h1>

				<a href="javascript:;" class="btPadraoExcluir">Excluir</a>
                
                <table id="tLista" cellpadding="0" cellspacing="0" border="0">
                
               

                    <tr>
                        <th width="5%"><input type="checkbox" class="checkbox" id="master"></th>
                        <th width="5%">ID</th>
                        <th width="5%">Foto</th>
                        <th class="tL">Nome</th>
                        <th width="15%">Dt Nasc</th>
                        <th width="25%">Email</th>
                        <th width="7%">Dep</th>
                        <th width="7%">St</th>
                    </tr>
                    @if($users->count())
                    @foreach ($users as $user)
                        @if ($loop->odd)
                    <tr id="tr_{{$user->id}}" bgcolor="#cddeeb">
                    	<td align="center" style="border-left:0;"><input name="chkStatus" type="checkbox" class="sub_chk" data-id="{{$user->id}}" /></td>
                    	<td align="center">{{ $user->id }}</td>
                        <td align="center"><img src="/storage/image/{{$user->foto}}" width="20" height="20" /></td>
                    	<td><a href="{{ route ('form.create') }}" class="linkUser" title="Clique aqui para editar este cadastro." id="nm_">{{ $user->nome }}</a></td>
                    	<td align="center">{{ $dtnasc}}</td>
                        <td align="center">{{ $user->email }}</td>
                        <td align="center">
                        	<a href="{{ route ('dependentes.create', $user->id ) }}" id="{{$user->id}}" class="btAdicionar" title="Adicionar dependentes para este cadastro."></a>
                        </td>
                    	<td align="center">
                        	<!--<a href="javascript:;" class="btVerde" title="Ativar/Desativar este cadastro." id="bol_0"></a>-->
                            <a href="#" class="btVerde" title="Ativar/Desativar este cadastro." name="status" id="bol_{{ $user->id }}" ></a>
                            
                        </td>
                    </tr>
                        @continue
                        @endif
                        @if ($loop->even)
                    <tr id="tr_{{$user->id}}" bgcolor="#f0f0f0">
                    	<td align="center" style="border-left:0;"><input name="chkStatus" type="checkbox" class="sub_chk" data-id="{{$user->id}}"  /></td>
                    	<td align="center">{{ $user->id }}</td>
                        <td align="center"><img src="/storage/image/{{$user->foto}}" width="20" height="20" /></td>
                    	<td><a href="form" class="linkUser" title="Clique aqui para editar este cadastro." id="nm_">{{ $user->nome }}</a></td>
                    	<td align="center">{{ $dtnasc }}</td>
                        <td align="center">{{ $user->email }}</td>
                        <td align="center">
                        
                        	<a href="{{ route ('dependentes.create', $user->id ) }}" class="btAdicionar" title="Adicionar dependentes para este cadastro."></a>
                            
                        </td>
                    	<td align="center">
                        
                            <a href="#" class="btCinza" title="Ativar/Desativar este cadastro." id="{{$user->id}}" name="status"  id="bol_{{ $user->id }}"></a>
                         
                        </td>
                    </tr>
                    
                        @endif
                    @endforeach
                    @endif
                </table>
              
            </div>

            <div id="paginacao">
                <a href="{{ $users->previousPageUrl() }}" class="btSeta1"></a> <div id="pags">1 de {{ $users->count() }}</div> <a href="{{ $users->nextPageUrl() }}" class="btSeta2"></a> 
              
                <select id="paginas">
                <option >1</option>
                <option value="{{ $users->nextPageUrl() }}">2</option>
                <option>3</option>
                <option>4</option>
                </select>

            </div>
          
        </div> <!-- FIM CONTEUDO DIR -->
    
    </div>

</div>
<script type="text/javascript"> 


$( ".btVerde" ).click(function() {
  $( this ).toggleClass( "btCinza" );
});

$( ".btCinza" ).click(function() {
  $( this ).toggleClass( "btVerde" );
  $( this ).removeClass( "btCinza" );
  $( this ).addClass( "btVerde" );
  $( this ).removeClass( "btCinza" );
});


    $(document).ready(function () {


        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        });


        $('.btPadraoExcluir').on('click', function(e) {


            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  


            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  


                var check = confirm("Você tem certeza que quer deletar?");  
                if(check == true){  


                    var join_selected_values = allVals.join(","); 


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });


                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });
                }  
            }  
        });


        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.trigger('confirm');
            }
        });


        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();


            $.ajax({
                url: ele.href,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });


            return false;
        });
    });
</script>   

</script>
</body>
</html>

