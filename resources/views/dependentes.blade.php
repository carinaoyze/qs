<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />

<link type="text/css" rel="stylesheet" href="{{ asset('principal.css') }}"> 

<title>Cadastro de Pessoas</title>
</head>

<body>

<div id="conteudoGeral">

    <div id="topoGeral">
    	<div id="logoTopo" onclick="location.href='index'" style="cursor:pointer;"></div>
    	<div id="dirTopo"></div>
    </div>
    
    <div id="baixoGeral">
    
    	<div id="menuEsq">
            <div id="titMenu">{{$menu}}</div>
            <a href="index">{{$inicio}}</a> 
            <a href="lista">{{$listar}}</a>
            <a href="{{ route ('form.create') }}"> {{$incluir}}</a>
        </div>
        
        <div id="conteudoDir">
        	
            <div id="listaPessoas">
            	<h1>Dependentes</h1>
                
                <div id="infoDep">

                    <div id="fotoCadastro">
                        <img src="/fotoCadastro.png" width="77" height="77" />
                    </div> 
                    
                    <table id="tListaCad" cellpadding="0" cellspacing="0" border="0">
                              

                        <tr>
                            <td class="tituloTab">Nome</td>    
                            <td>Steves Jobs</td>    
                        </tr>              
                        <tr bgcolor="#cddeeb">
                            <td class="tituloTab">Data de Nascimento</td>    
                            <td>25/08/1964</td>    
                        </tr>              
                        <tr>
                            <td class="tituloTab">Email</td>    
                            <td>steve.jobs@apple.com</td>    
                        </tr>              
                    </table>
                    
                    <form id="frmAdicionaDep" method="post" enctype="multipart/form-data" action="{{ route('dependentes.store') }}">
                    {{ csrf_field() }}
                        <div class="agrupa mB mR">
                            <label for="cNomeDep">Nome</label><br />
                            <input type="text" name="cNomeDep" id="cNomeDep" />
                        </div>    
                        <div class="agrupa">
                            <label for="cDataNasc">Data de Nascimento</label><br />
                            <input type="text" name="cDataNasc" id="cDataNasc" />
                          
                        </div>                            
                        <br><br>
                        @if ($errors->any())
                        <div>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
	                    <a href="javascript:frmAdicionaDep.submit();" class="btPadrao">Adicionar</a>
                   
                    </form>
                
                    
                    <table id="tLista" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <th width="60%" class="tL">Nome do Dependente</th>
                            <th width="33%">Data de Nascimento</th>
                            <th></th>
                        </tr>    
                        @foreach ($users as $user)
                        
                            @if ($loop->even)
                            <tr>
                                <td>{{ $user->nome }}</td>
                                <td align="center">{{$user->dtnasc }}</td>
                                <td align="center"><a href="" class="btRemover"></a></td>
                            </tr>
                            @continue
                            @endif
                            @if ($loop->odd)    
                        <tr bgcolor="#cddeeb">
                            <td>{{ $user->nome }}</td>
                            <td align="center">{{ $user->dtnasc }}</td>
                            <td align="center"><a href="" class="btRemover"></a></td>
                        </tr>
                            @endif
                        
                        @endforeach
                    </table>
                    
                    <a href="javascript:;" class="btPadrao mT">Salvar</a>
                </div>
                
            </div>    

        </div> <!-- FIM CONTEUDO DIR -->
    
    </div>

</div>

</body>
</html>

