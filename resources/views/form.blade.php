<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />

<link type="text/css" rel="stylesheet" href="{{ asset('principal.css') }}">


<title>Cadastro de Pessoas</title>
</head>

<body>

<div id="conteudoGeral">

    <div id="topoGeral">
    	<div id="logoTopo" onclick="location.href='index'" style="cursor:pointer;"></div>
    	<div id="dirTopo"></div>
    </div>
    
    <div id="baixoGeral">
    
    	<div id="menuEsq">
            <div id="titMenu">{{$menu}}</div>
            <a href="index">{{$inicio}}</a> 
            <a href="lista">{{$listar}}</a>
            <a href="{{ route ('form.create') }}"> {{$incluir}}</a>
        </div>
        
        <div id="conteudoDir">

            <div id="listaPessoas">
            
                <h1>Incluindo um Novo Cadastro</h1>
               
                <form id="formCadastrar" method="post" enctype="multipart/form-data" action="{{ route('form.store') }}">
                    {{ csrf_field() }}
                    <label for="cNome">Nome</label><br />
                    <input id="cNome" name="cNome" value="{{old('cNome')}}" required/><br />

                    <label for="cDataNasc">Data de Nascimento (Formato:dia/mês/ano)</label><br />
                    <input id="cDataNasc" name="cDataNasc" value="{{old('cDataNasc')}}" required /><br />
                    
                    <label for="cEmail">E-Mail</label><br />
                    <input id="cEmail" name="cEmail" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" value="{{old('cEmail')}}" required/><br />
                    <label for="cFoto">Foto (somente .jpg - máximo de 200Kb)</label><br />
                    <input id="cFoto" name="cFoto" type="file" accept="image/jpeg" data-max-size="2000"/><br />
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <a href="javascript:formCadastrar.submit()"  class="btPadrao">Salvar</a>
                </form>
               
                
            
            </div>

        </div> <!-- FIM CONTEUDO DIR -->
    
    </div>

</div>

</body>
</html>


