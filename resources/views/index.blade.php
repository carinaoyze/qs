<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />

<link type="text/css" rel="stylesheet" href="{{ asset('/principal.css') }}"> 

<title>Cadastro de Pessoas!</title>
</head>

<body>

<div id="conteudoGeral">

    <div id="topoGeral">
    	<div id="logoTopo" onclick="location.href='index'" style="cursor:pointer;"></div>
    	<div id="dirTopo"></div>
    </div>
    
    <div id="baixoGeral">
    
    	<div id="menuEsq">
            <div id="titMenu">{{$menu}}</div>
            <a href="index">{{$inicio}}</a> 
            <a href="lista">{{$listar}}</a>
            <a href="{{ route ('form.create') }}"> {{$incluir}}</a>
        </div>
        
        <div id="conteudoDir">

            <div id="listaPessoas">
            	<h1>Início</h1>
                Utilize os itens do menu a esquerda para navegar no sistema.
			</div>

        </div> <!-- FIM CONTEUDO DIR -->
    
    </div>

</div>

</body>
</html>