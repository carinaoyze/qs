<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Form;

class Dependentes extends Model
{
    use HasFactory;

    protected $table = 'dependentes';

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function form() {
        return $this->belongsTo(Form::class);
    }
}
