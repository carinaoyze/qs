<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Dependentes;

class Form extends Model
{
    use HasFactory;

    protected $table = 'cadastro';

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function Dependentes() {
        return $this->hasMany(Dependentes::class);
    }
   
}

    

