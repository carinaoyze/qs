<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cNome' => 'required|string',
            'cDataNasc' => 'required|date_format:d/m/Y|after_or_equal:01/01/1901',
            'cEmail' => 'required|email:rfc,dns',
            'cFoto' => 'image|mimes:jpeg, jpg|nullable|max:200',
            //
        ];
    }

    public function messages()
    {
        return [
            'cNome.required' => 'O campo nome é obrigatório.',
            'cDataNasc.required' => 'O campo data de nascimento é obrigatório',
            'cEmail.required' => 'O campo email é obrigatório',
            'cFoto.file' => 'O campo foto deve ter no máximo 200kb.',
            //
        ];
    }
}
