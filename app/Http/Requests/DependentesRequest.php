<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DependentesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cNomeDep' => 'required|string',
            'cDataNasc' => 'required|date_format:d/m/Y|after_or_equal:01/01/1901',
        
            //
        ];
    }

    public function messages()
    {
        return [
            'cNomeDep.required' => 'O campo nome é obrigatório.',
            'cDataNasc.required' => 'O campo data de nascimento é obrigatório',
            
            //
        ];
    }
}
