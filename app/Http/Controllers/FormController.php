<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\CadastroRequest;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
       
               
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/form', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CadastroRequest $request)
    {
              
        
        if ($request->hasFile('cFoto')) {
            $filenameWithExt = $request->file('cFoto')->getClientOriginalName ();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('cFoto')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = $filename. '_'. time().'.'.$extension;
            // Upload Image$path 
            $request->file('cFoto')->storeAs('public/image', $fileNameToStore);
            }
            // Else add a dummy image
            else {
            $fileNameToStore = 'noimage.jpg';
            }


        DB::table('cadastro')->insert([
                       'nome'=>$request->input('cNome'),
                       'email'=>$request->input('cEmail'),
                       'dtnasc'=>Carbon::createFromFormat('d/m/Y', $request->input('cDataNasc'))->format('Y-m-d'),
                       'Foto'=>$fileNameToStore,
                       ]);

        return view('recebido', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
        return view('form.edit', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo']);

        DB::table('cadastro')
              ->where('id', 1)
              ->update(['nome'=>$request->input('cNome'),
              'email'=>$request->input('cEmail'),
              'dtnasc'=>Carbon::createFromFormat('d/m/Y', $request->input('cDataNasc'))->format('Y-m-d'),
              'Foto'=>$fileNameToStore,]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
