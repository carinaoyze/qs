<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use IlluminateHttpRequest;
use IlluminatePaginationPaginator;
use IlluminateSupportCollection;
use IlluminatePaginationLengthAwarePaginator;
use Livewire\Component;

class ListaController extends Controller
{
  

    public function render() 
    {
        $dtnasc = DB::table('cadastro')->value('dtnasc');
        $format = Carbon::createFromFormat('Y-m-d', $dtnasc)->format('d/m/Y');
        //$users = \App\Models\User::paginate(3);
      
        return view('lista', [
        'menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo',
        'dtnasc'=> $format,
        'users' => DB::table('cadastro')->paginate(3),
        
        ]);

        
       
       
    }

    public function destroy($id)
    {
    	DB::table("cadastro")->delete($id);
    	return response()->json(['success'=>"Cadastro deletado com sucesso.", 'tr'=>'tr_'.$id]);
    }
    
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("cadastro")->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Cadastros deletados com sucesso."]);
    }

}
