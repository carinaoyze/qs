<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Form;
use App\Models\Dependentes;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use IlluminateHttpRequest;
use IlluminatePaginationPaginator;
use IlluminateSupportCollection;
use IlluminatePaginationLengthAwarePaginator;
use App\Http\Requests\DependentesRequest;
use Livewire\Component;

class DependentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dtnasc = DB::table('dependentes')->value('dtnasc');
        return view('dependentes', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo',
        'users' => DB::table('dependentes')->paginate(3),
        ]);

        $form = Form::all();
        $dependentes = dependentes::all();

        return view('dependentes.dependentes', 
       compact('form','dependentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $forms = DB::table('dependentes')->get();
        $dtnasc = DB::table('dependentes')->value('dtnasc');
        return view('dependentes', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo',
        'users' => DB::table('dependentes')->paginate(3),
        ]);

        
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DependentesRequest $request)
    {
        
        return view('dependentes', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo',
        'users' => DB::table('dependentes')->paginate(3)]);
        
        $forms = Form::with('dependentes')->get();
        DB::table('dependentes')->insert([
            'nome'=>$request->input('cNomeDep'),
            'dtnasc'=>Carbon::createFromFormat('d/m/Y', $request->input('cDataNasc'))->format('Y-m-d'),
            'id_cadastro'=>  $form
            ]);
            $dtnasc = DB::table('dependentes')->value('dtnasc');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dtnasc = DB::table('dependentes')->value('dtnasc');
        return view('dependentes', ['menu' => 'Menu de Opções', 
        'inicio' => 'Início', 
        'listar' => 'Listar Cadastros', 
        'incluir' => 'Incluir Novo']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function someMethod($idd) {
        echo $idd;
    }
}
